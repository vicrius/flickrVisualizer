//
//  PhotoUtils.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 03/06/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import Foundation

enum PhotoSize: String{
    
    //small, 240 on longest side
    case TypeSmall = "m"
    //medium 640, 640 on longest side
    case TypeMedium = "z"
}

class PhotoUtils {
    
    static func getUrl(farm: AnyObject ,serverId: AnyObject,photoId: AnyObject ,secret: AnyObject, imageSize: PhotoSize) -> String {
        let newUrl: String = "https://farm\(farm).staticflickr.com/\(serverId)/\(photoId)_\(secret)_\(imageSize.rawValue).jpg"
        return newUrl;
    }
}
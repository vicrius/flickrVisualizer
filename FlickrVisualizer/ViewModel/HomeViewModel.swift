//
//  HomeViewModel.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 31/05/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import Foundation
import UIKit
import FlickrKit

protocol HomeViewModelDelegate: class {
    
    func reloadCollectionView();
}

class HomeViewModel {
    
    weak var delegate: HomeViewModelDelegate?;
    
    var userId: String = "peterbrannon";
    var arrayOfImages = [ImageDetails]();
    
    func loadDataFromFlickrPhotosSearch() {
        
        FlickrKit.sharedFlickrKit().call("flickr.photos.search", args: ["user_id": userId]) { (results, error) in
            
            if (results != nil) {
                if let photos = results["photos"] as? [String: AnyObject], let photo = photos["photo"] as? Array <AnyObject>
                {
                    for image in photo {
                        if let farm = image["farm"], let farm2 = farm, let serverId = image["server"], let serverId2 = serverId, let photoId = image["id"], let photoId2 = photoId, let secret = image["secret"], let secret2 = secret {
                            
                            let smallUrl = PhotoUtils.getUrl(farm2, serverId: serverId2, photoId: photoId2, secret: secret2, imageSize: PhotoSize.TypeSmall);
                            let currentImageDetails = ImageDetails(photoId: photoId2 as! String, stringUrl: smallUrl, photoTitle: "", location: "", realname: "", username: "")
                            self.arrayOfImages.append(currentImageDetails);
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.delegate?.reloadCollectionView();
                    });
                }
            }
        }
    }
    
    func numberOfSections() -> Int {
        return 1;
    }
    
    func numberOfItemsInSection(section: Int = 0) -> Int {
        return arrayOfImages.count;
    }
    
    func detailsForItemAtIndexPath(indexPath: NSIndexPath) -> ImageDetails {
        let itemRow = arrayOfImages[indexPath.row];
        return itemRow;
    }
}
//
//  DetailViewModel.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 02/06/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import Foundation
import UIKit
import FlickrKit

protocol DetailViewModelDelegate: class {
    
    func reloadDetails();
}

class DetailViewModel {
    
    weak var delegate: DetailViewModelDelegate?;
    let selectedImageDetails = ImageDetails();
    var imageData: NSData?;
    
    func loadPhotoDetailsFromFlickrPhotosGetInfo(photoId: String) {
        
        self.selectedImageDetails.photoId = photoId;
        
        FlickrKit.sharedFlickrKit().call("flickr.photos.getInfo", args: ["photo_id": photoId]) { (results, error) in
            
            if let photo: AnyObject = results["photo"]{
                
                if let farm = photo["farm"], let farm2 = farm, let serverId = photo["server"], let serverId2 = serverId, let photoId = photo["id"], let photoId2 = photoId, let secret = photo["secret"], let secret2 = secret {
                    
                    let mediumUrl = PhotoUtils.getUrl(farm2, serverId: serverId2, photoId: photoId2, secret: secret2, imageSize: PhotoSize.TypeMedium);
                    self.selectedImageDetails.stringUrl = mediumUrl;
                    self.loadImageFromUrl(mediumUrl);
                }
                
                if let title: AnyObject = photo["title"] {
                    if let titleValue = title["_content"], let titleValue2 = titleValue {
                        
                        self.selectedImageDetails.photoTitle = titleValue2 as! String;
                    }
                }
                
                if let owner: AnyObject = photo["owner"] {
                    
                    if let locationValue = owner["location"], let locationValue2 = locationValue {
                        
                        self.selectedImageDetails.location =  "\(locationValue2)";
                        
                    }
                    if let realnameValue = owner["realname"], let realnameValue2 = realnameValue, let usernameValue = owner["username"], let usernameValue2 = usernameValue {
                        
                        self.selectedImageDetails.realname =  "\(realnameValue2)";
                        self.selectedImageDetails.username =  "\(usernameValue2)";
                        
                    }
                }
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.delegate?.reloadDetails();
                    
                });
            }
        }
    }
    
    func loadImageFromUrl(url: String) {
        
        if let nsUrl = NSURL(string: url), let nsdataUrl = NSData(contentsOfURL: nsUrl) {
            self.imageData = nsdataUrl;
        }
    }
}
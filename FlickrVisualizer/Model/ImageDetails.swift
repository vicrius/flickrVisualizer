//
//  ImageDetails.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 31/05/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import UIKit

class ImageDetails {
    
    var photoId: String;
    var stringUrl: String;
    var photoTitle: String;
    var location: String;
    var realname: String;
    var username: String;

    init(photoId: String, stringUrl: String, photoTitle: String, location: String, realname: String, username: String) {
        
        self.photoId = photoId;
        self.stringUrl = stringUrl;
        self.photoTitle = photoTitle;
        self.location = location;
        self.realname = realname;
        self.username = username;
    }
    
    init() {
        self.photoId = "";
        self.stringUrl = "";
        self.photoTitle = "";
        self.location = "";
        self.realname = "";
        self.username = "";
    }
}
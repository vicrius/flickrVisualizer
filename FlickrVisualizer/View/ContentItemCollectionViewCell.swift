//
//  ContentItemCollectionViewCell.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 31/05/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import UIKit

class ContentItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgCell: UIImageView!
    @IBOutlet var activityIndicatorCell: UIActivityIndicatorView!

}
//
//  DetailViewController.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 01/06/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, DetailViewModelDelegate {
    
    private let viewModel: DetailViewModel = DetailViewModel();
    
    @IBOutlet var imgDetailView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var lbLocation: UILabel!
    @IBOutlet var lbTakenBy: UILabel!
    
    var photoId: String?;
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self;
        
        self.activityIndicator.startAnimating();
        
        if let photoId = self.photoId {
            self.viewModel.loadPhotoDetailsFromFlickrPhotosGetInfo(photoId);
        }
        self.loadDetails();
    }
    
    func reloadDetails() {
        self.loadDetails();
        self.imgDetailView.contentMode = .ScaleAspectFit;
        self.imgDetailView.backgroundColor = UIColor.clearColor();
        self.activityIndicator.stopAnimating();
        
    }
    
    func loadDetails() {
        self.navigationItem.title = self.viewModel.selectedImageDetails.photoTitle;
        
        if let image = self.viewModel.imageData {
            
            self.imgDetailView.image = UIImage(data: image);
            self.lbLocation.text = "Location: \(self.viewModel.selectedImageDetails.location)";
            self.lbTakenBy.text = "Taken by: \(self.viewModel.selectedImageDetails.realname) (\(self.viewModel.selectedImageDetails.username))";
            
        }
    }
}

//
//  HomeViewController.swift
//  FlickrVisualizer
//
//  Created by Víctor Rius on 31/05/16.
//  Copyright © 2016 Víctor Rius. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, HomeViewModelDelegate {
    
    private let viewModel: HomeViewModel = HomeViewModel();
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        let cellNib = UINib.init(nibName: "ContentItemCollectionViewCell", bundle: nil);
        self.collectionView.registerNib(cellNib, forCellWithReuseIdentifier: NSStringFromClass(ContentItemCollectionViewCell));
        
        navigationItem.title = ("\(viewModel.userId)'s photos");
        activityIndicator.startAnimating();
        viewModel.loadDataFromFlickrPhotosSearch();
    }
    
    func reloadCollectionView()
    {
        activityIndicator.stopAnimating();
        collectionView.reloadData();
    }
    
    // MARK: UICollectionViewDataSource functions
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.viewModel.numberOfSections();
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItemsInSection();
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NSStringFromClass(ContentItemCollectionViewCell), forIndexPath: indexPath) as! ContentItemCollectionViewCell;
        
        let stringUrl = self.viewModel.arrayOfImages[indexPath.row].stringUrl;
        
        if let url = NSURL(string: stringUrl) {
            
            cell.imgCell.image = nil;
            cell.activityIndicatorCell.startAnimating();
            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                
                guard let imagedData = NSData(contentsOfURL: url) else {
                    return
                }
                let image = UIImage(data: imagedData)
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    cell.imgCell.image = image;
                    cell.activityIndicatorCell.stopAnimating();
                }
            }
        }
        
        return cell;
        
    }
    
    // MARK: UICollectionViewDelegate functions
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("segue_detailViewController", sender: indexPath);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let identifier = segue.identifier else {
            return;
        }
        
        if identifier == "segue_detailViewController" {
            let detailViewController =  segue.destinationViewController as! DetailViewController
            
            if let item = sender as? NSIndexPath {
                detailViewController.photoId = self.viewModel.arrayOfImages[item.row].photoId;
                
            }
        }
        
    }
}

